#! /bin/bash

#
# install mysql
#

major=5.6
version=5.6.14
tarball=mysql-${version}.tar.gz
archivedir=mysql-${version}

if [ ! -f ${tarball} ];then
    wget "http://dev.mysql.com/get/Downloads/MySQL-${major}/${tarball}/from/http://cdn.mysql.com/" -O ${tarball}
fi

tar zxvf ${tarball}
pushd ${archivedir}

cmake . \
    -DDEFAULT_CHARSET=utf8 \
    -DDEFAULT_COLLATION=utf8_general_ci \
    -DCMAKE_INSTALL_PREFIX=/usr/local/mysql;

make && make test && make install

groupadd -g 3306 mysql
useradd -M -u 3306 -g mysql mysql

[ -e /etc/init.d/mysql ] || cp -af /usr/local/mysql/support-files/mysql.server /etc/init.d/mysql
[ -e /etc/my.cnf ] || cp -af /usr/local/mysql/support-files/my-default.cnf /etc/my.cnf
/usr/local/mysql/scripts/mysql_install_db --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data --user=mysql

chown -R root:mysql /usr/local/mysql
chown -R mysql:mysql /usr/local/mysql/data

popd
rm -fr ${archivedir}

[ -e /etc/ld.so.conf.d/mysql.conf ] || echo "/usr/local/mysql/lib" > /etc/ld.so.conf.d/mysql.conf
ldconfig


exit 0;

