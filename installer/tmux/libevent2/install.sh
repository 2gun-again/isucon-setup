#! /bin/bash

#
# install libevent2
#

version=2.0.21
tarball=libevent-${version}-stable.tar.gz
archivedir=libevent-${version}-stable

tar zxvf ${tarball}

pushd ${archivedir}

./configure  \
    --prefix=/usr/local \
;

make && make install

popd

rm -fr ${archivedir}

exit 0;


