#! /bin/bash

#
# setup git-daemon on supervisord
#

groupadd -g 9418 git
useradd -M -s /bin/false -u 9418 -g git git

sudo  mkdir -p /home/git/tmp /home/git/repos

sudo cp -af ./git-daemon.ini /usr/local/supervisord/conf/supervisord.d/
sudo cp -af ./newrepo.sh /home/git/

chown git:git /home/git/newrepo.sh

exit 0;
