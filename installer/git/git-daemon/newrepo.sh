#! /bin/bash

#
# create new git repository
#

if [ $# -lt 1 ];then
    cat << 'EOS'
    Usage:
        ./newrepo.sh [reposiroty_name]
EOS
   exit 1; 
fi

git_home=/home/git

repo=$1

mkdir ${git_home}/tmp/${repo}

pushd ${git_home}/tmp/${repo}
touch ${git_home}gitignore

git init 
git add .gitignore
git commit -m 'init'

popd

git clone --bare ${git_home}/tmp/${repo} ${git_home}/repos/${repo}.git

rm -fr /tmp/${repo}


exit 0;

