#! /bin/bash

#
# zsh install
#

version=5.0.2

tarball=zsh-${version}.tar.gz
archivedir=zsh-${version}

if [ ! -f ${tarball} ];then
    wget http://sourceforge.net/projects/zsh/files/zsh/${version}/${tarball}/download -O ${tarball}
fi

tar zxvf ${tarball}

cd ${archivedir}

./configure \
    --prefix=/usr/local/ \
    --enable-FEATURE=yes \
    --enable-zsh-debug  \
    --enable-zsh-mem \
    --enable-zsh-mem-debug \
    --enable-zsh-mem-warning \
    --enable-zsh-secure-free \
    --enable-etcdir=/etc/ \
    --enable-zshenv=/etc/zshenv \
    --enable-zshrc=/etc/zshrc \
    --enable-zprofile=/etc/zprofile \
    --enable-zlogin=/etc/zlogin \
    --enable-zlogout=/etc/zlogout \
    --disable-restricted-r  \
    --disable-locale  \
    --enable-function-subdirs \
    --enable-additional-fpath=/etc/zshfunctions \
    --enable-scriptdir=/etc/zshscripts \
    --enable-max-function-depth=1024 \
    --enable-pcre \
    --enable-cap  \
    --disable-gdbm \
    --enable-multibyte \
    --enable-libc-musl;

make && make install

cd ../ && rm -irf $archivedir

[ $(grep -e "^/usr/local/bin/zsh$" /etc/shells | wc -l | perl -pe 's/\s//g') = '0' ] && echo /usr/local/bin/zsh >> /etc/shells

exit 0;
