#! /bin/sh

#
# install nginx
#

current=$(cd $(dirname $0) && pwd)

cd ${current}

version=1.4.2
tarball=nginx-${version}.tar.gz
archivedir=nginx-${version}

if [ ! -f ${tarball} ];then
    echo "downloading ${tarball}"
    wget http://nginx.org/download/${tarball}
fi

useradd -s/sbin/nologin -d/usr/local/nginx -M nginx || echo 'user exist'

tar xzvf ${tarball}
cd ${archivedir}

./configure \
    --prefix=/usr/local/nginx \
    --without-mail_pop3_module \
    --without-mail_imap_module \
    --without-mail_smtp_module \
    --with-file-aio \
    --with-ipv6 \
    --with-http_ssl_module \
    --with-http_realip_module \
    --with-http_addition_module \
    --with-http_xslt_module \
    --with-http_sub_module \
    --with-http_dav_module \
    --with-http_flv_module \
    --with-http_mp4_module \
    --with-http_gzip_static_module \
    --with-http_random_index_module \
    --with-http_secure_link_module \
    --with-http_degradation_module \
    --with-http_stub_status_module \
    --with-debug \
    --user=nginx; 

make upgrade || echo 'nginx already down'
make && make install

cd ${current} && rm -irf ${archivedir}


if [ $(uname -s) = 'Linux' ];then
    initscript=${current}/initscripts/rhel.sh
    if [ -e /etc/debian_version ];then
        initscript=${current}/initscripts/debian.sh
    fi
    cp -af ${initscript} /etc/init.d/nginx
fi

exit 0
